/**
 * Created by Jeff on 2/7/2016.
 */
import * as assert from 'assert';
import {Utility} from '../lib/utility';

export class User {
	constructor(args) {
		assert.ok(args.email, 'Email is required');
		this.email = args.email;
		this.createdAt = args.createdAt || new Date();
		this.status = args.status || 'pending';
		this.signInCount = args.signInCount || 0;
		this.lastLoginAt = args.lastLoginAt || new Date();
		this.currentLoginAt = args.currentLoginAt || new Date();
		this.currentSessionToken = args.currentSessionToken || null;
		this.reminderSentAt = args.reminderSentAt || null;
		this.reminderToken = args.reminderToken || null;
		this.authenticationToken = args.authenticationToken || Utility.randomString(18);
	}
}
