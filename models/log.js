/**
 * Created by Jeff on 2/7/2016.
 */
import * as assert from 'assert';

export class Log {
	constructor(args) {
		assert.ok(args.subject && args.entry && args.userId, 'Need subject, entry, and userId');
		this.subject = args.subject;
		this.entry = args.entry;
		this.createdAt = new Date();
		this.userId = args.userId;
	}
}
