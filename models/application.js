/**
 * Created by Jeff on 2/7/2016.
 */

export class Application {
	constructor (args) {
		this.email = args.email;
		this.password = args.password;
		this.confirm = args.confirm;
		this.status = 'pending';
		this.message = null;
	}
	isValid() {
		return this.status = 'validated';
	}
	setInvalid(message) {
		this.status = 'invalid';
		this.message = message;
	}
	isInvalid() {
		return !this.isValid();
	}
	validate() {
		this.status = 'validated'
	}
}
