import {expect} from 'chai';
import {it, before} from 'arrow-mocha/es5';
import {User} from '../models/user';

describe('User', () => {
	describe('defaults', () => {
		before(t => {
			t.user = new User({email: 'rob@tekpub.com'});
		});

		it('email is rob@tekpub.com', t => {
			expect(t.user.email).to.equal('rob@tekpub.com');
		});
		it('has an authentication token', t => {
			expect(t.user.authenticationToken).to.be.ok;
		});
		it('has a pending status', t => {
			expect(t.user.status).to.equal('pending');
		});
		it('has a created date', t => {
			expect(t.user.createdAt).to.be.ok;
		});
		it('has a signInCount of 0', t => {
			expect(t.user.signInCount).to.equal(0);
		});
		it('has lastLogin', t => {
			expect(t.user.lastLoginAt).to.be.ok;
		});
		it('has currentLogin', t => {
			expect(t.user.currentLoginAt).to.be.ok;
		});
	});
});
