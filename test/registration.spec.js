/**
 * Created by Jeff on 2/7/2016.
 */
import chai from 'chai';
import chaiAsPromised from 'chai-as-promised';
import {it, before} from 'arrow-mocha/es5';
import {Registration} from '../lib/registration';
import {User} from '../models/user';
import {db} from '../lib/db';

chai.use(chaiAsPromised);
const expect = chai.expect;

describe('Registration', () => {
	// happy path
    describe('a valid application', () => {
		before((t) => {
			t.regResult = Registration.applyForMembership({
				email: 'rob@tekpub.com'
			});
		});

		it('is successful', t => {
			expect(Promise.resolve(t.regResult.success)).to.eventually.be.true;
		});
		it('creates a user', t => {
			expect(Promise.resolve(t.regResult.user)).to.eventually.be.ok;
		});
		it('creates a log entry');
		it('sets the user\'s status to approved');
		it('offers a welcome message');
	});

	describe('empty or null email', () => {
		it('is not successful');
		it('tells the user that email is required');
	});

	describe('empty or null password', () => {
		it('is not successful');
		it('tells the user that password is required');
	});

	describe('password and confirm mismatch', () => {
		it('is not successful');
		it('tells the user that passwords do not match');
	});

	describe('email already exists', () => {
		it('is not successful');
		it('tells the user that email already exists');
	});
});
