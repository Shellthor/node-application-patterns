/**
 * Created by Jeff on 2/7/2016.
 */
import assert from 'assert';
import {User} from '../models/user';
import {Application} from '../models/application';
import {db} from '../lib/db';
import 'bluebird';

class RegResult {
	constructor(args) {
		this.success = false;
		this.message = null;
		this.user = null;
	}
}

function validateInputs(app) {
	return new Promise((resolve, reject) => {
        if(!app.email || !app.password) {
            app.setInvalid('Email and password are required');
        } else if (app.password !== app.confirm) {
            app.setInvalid('Passwords don\'t match');
        } else {
            app.validate();
        }
        resolve(app);
    });
}

function checkIfUserExists(app) {
    return new Promise((resolve, reject) => {
        const users = db.collection('users');
        users.findOne({email: app.email}).then(user => {
            if(user) {
                app.setInvalid('Email already exists');
            }
            resolve(app);
        }).catch(err => {
            reject(err);  
        });
        
    });
}

export class Registration {
	static applyForMembership(args) {
		return new Promise((resolve, reject) => {
            const regResult = new RegResult();
            const app = new Application(args);

            //validate inputs
            validateInputs(app)
                .then(checkIfUserExists)
                .then(app => {
                    console.log("Application: " + app);
                    console.log('regResult: ' + regResult);
                    if(app.isValid()) {
                        regResult.success = false;
                        regResult.message = 'Welcome!';
                        regResult.user = new User(args);
                    }
                })
                .catch(reason => {
                    console.log(reason);
                });
            //create a new user

            
            //hash the password
            //create a log entry
            
            resolve(regResult);
        });
	}
}
