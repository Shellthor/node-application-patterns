/**
 * Created by Jeff on 2/7/2016.
 */
export class Utility {
	static randomString(stringLength) {
		stringLength = stringLength || 12;
		const chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwzyz';
		let result = '';
		for (let i = 0; i < stringLength; i++) {
			const rnum = Math.floor(Math.random() * chars.length);
			result += chars.substring(rnum, rnum + 1);
		}
		return result;
	}
}
