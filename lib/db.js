import mongodb from 'mongodb-promises';
import 'bluebird'

const db = mongodb.db('localhost:27017', 'membership');

export {db};
